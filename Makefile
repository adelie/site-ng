DIR_NAME := $(notdir $(CURDIR))
GIT_HASH := $(shell git rev-list HEAD^!)

all:

site:
	$(CURDIR)/publish sass logo html
logo:
	$(CURDIR)/publish logo
	rm -f $(CURDIR)/$(DIR_NAME)-images-${GIT_HASH}.tar.gz
	tar -C $(CURDIR)/assets/images -pczf $(CURDIR)/$(DIR_NAME)-images-${GIT_HASH}.tar.gz .

clean:
	rm -fr $(CURDIR)/html
	find $(CURDIR) -mindepth 1 -maxdepth 1 -type f -name '*.tar.gz' -delete
