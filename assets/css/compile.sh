#!/bin/sh -e

HERE=$(dirname $(readlink -f ${0}));
NODE=23.9.0-alpine3.21; # https://hub.docker.com/_/node

cd ${HERE};

#===============================================================
docker run -v${HERE}:/x -w/x --rm -i node:${NODE} sh <<'EOF'
set -e;
set -x
apk update;
apk upgrade;
apk add git;
#===============================================================

cd /tmp;
git clone https://github.com/sass/sass.git;
cd sass;
git checkout 1c9ec00bbb061fa1be92c2e8c81144e6b8d8159b;
npm audit fix || true;
npm install;
npm install -g sass;
cd /x;

sass --no-source-map scss/theme.scss theme.css;
chown 1000:1000 theme.css;

#===============================================================
EOF
#===============================================================

rm -fr fonts src;
mkdir src;
mv theme.css src;

#===============================================================
docker run -u$(id -u):$(id -g) -v${HERE}:/x -w/x --rm -i node:${NODE} sh <<'EOF'
set -e;
#===============================================================

npm install;
npm run build;

#===============================================================
EOF
#===============================================================

mv css/theme.css .;
rm -fr css;

rm -fr ../fonts;
mv fonts ..;

rm -fr node_modules;
rm -fr src;
