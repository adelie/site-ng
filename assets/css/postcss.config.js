module.exports = _ => {
  return {
    plugins: [
      // required for IE
      require('postcss-selector-not').default,
      // converts googleapis URLs to gstatic ones
      require('postcss-import-url')({
        recursive: true,
        modernBrowser: true,
      }),
      // downloads gstatic fonts for local serving
      require('postcss-font-grabber').postcssFontGrabber({
        cssSrc: 'src',
        cssDest: 'css',
        fontDest: 'fonts',
      }),
      // main compatibility phase
      require('postcss-preset-env')({
        stage: 3,
      }),
      // must be run before autoprefixer
      require('postcss-unprefix')(),
      require('autoprefixer')(),
      // does not /add/ prefixes by default, hence previous step
      require('cssnano')({
        preset: 'advanced'
      }),
    ]
  };
};
