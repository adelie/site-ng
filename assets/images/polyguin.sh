#!/bin/sh -e

HERE=$(dirname $(readlink -f ${0}));
cd ${HERE};

rm -f gen_*;

#---------------------------------------------------------------
# Transparent monochrome (B on W, W on B) SVGs from master.

##
# Target: color
#
# Optionally apply color transformations here.
#
cp polyguin.svg gen_polyguin_color.svg

##
# Target: black
#
# Convert color SVG to black SVG and maintain transparency.
#
cp polyguin.svg gen_polyguin_black.svg
grep fill: polyguin.svg                                        \
    | grep -v fill:none                                        \
    | grep -E -o '\#[a-f0-9]*\w'                               \
    | sort                                                     \
    | uniq                                                     \
    | cut -d\# -f2                                             \
    | while read k; do
          sed -i gen_polyguin_black.svg                        \
              -e 's@'"$k"'@__BLACK_'"$k"'__@g'                 \
              ;
      done                                                     \
    ;
grep fill: polyguin.svg                                        \
    | grep -v fill:none                                        \
    | grep -E -o '\#[a-f0-9]*\w'                               \
    | sort                                                     \
    | uniq                                                     \
    | cut -d\# -f2                                             \
    | sed 's/.\{2\}/& /g'                                      \
    | awk '{ print "0x"$1,"0x"$2,"0x"$3, ($1 $2 $3) }'         \
    | awk --non-decimal-data '{ print $4, (1 - (($1 + $2 + $3) / (3 * 256))) }' \
    | while read a b; do
          sed -i gen_polyguin_black.svg                        \
              -e '/'"$a"'/ s@-opacity:1@-opacity:'"$b"'@g'     \
              -e '/'"$a"'/ s@__BLACK_[a-f0-9]*__@000000@g'     \
              ;
      done                                                     \
    ;


##
# Target: white
#
# Convert color SVG to white SVG and maintain transparency.
#
cp polyguin.svg gen_polyguin_white.svg
grep fill: polyguin.svg                                        \
    | grep -v fill:none                                        \
    | grep -E -o '\#[a-f0-9]*\w'                               \
    | sort                                                     \
    | uniq                                                     \
    | cut -d\# -f2                                             \
    | while read k; do
          sed -i gen_polyguin_white.svg                        \
              -e 's@'"$k"'@__WHITE_'"$k"'__@g'                 \
              ;
      done                                                     \
    ;
grep fill: polyguin.svg                                        \
    | grep -v fill:none                                        \
    | grep -E -o '\#[a-f0-9]*\w'                               \
    | sort                                                     \
    | uniq                                                     \
    | cut -d\# -f2                                             \
    | sed 's/.\{2\}/& /g'                                      \
    | awk '{ print "0x"$1,"0x"$2,"0x"$3, ($1 $2 $3) }'         \
    | awk --non-decimal-data '{ print $4, (($1 + $2 + $3) / (3 * 256)) }' \
    | while read a b; do
          sed -i gen_polyguin_white.svg                        \
              -e '/'"$a"'/ s@-opacity:1@-opacity:'"$b"'@g'     \
              -e '/'"$a"'/ s@__WHITE_[a-f0-9]*__@ffffff@g'     \
              ;
      done                                                     \
    ;


#===============================================================
docker run -v$(pwd):/x -w/x --rm -i alpine:3.21 <<'EOF'
apk add graphicsmagick imagemagick imagemagick-svg;
#===============================================================

#---------------------------------------------------------------
# SVG master --> scaled PNGs for composition onto logo template.

##
# SVG --> PNG for master logo icon.
#
#  x28 is GitLab
#  x54 is Website
# x200 is SPI project page
# x900 is WordPress theme
#
# This runs inside Docker because of ImageMagick bugs and GM is
# the only free software I can find that doesn't screw this up.
#
for c in color black white; do
for r in x28 x54 x200 x900; do
    gm convert -trim -density 600 -resize ${r} -background none gen_polyguin_${c}.svg gen_polyguin_${c}_${r}.png;
    magick convert -auto-level -brightness-contrast -5x+25 gen_polyguin_${c}_${r}.png gen_polyguin_${c}_${r}.png;
    chown 1000:1000 gen_polyguin_${c}_${r}.png;
done # r
done # c


#---------------------------------------------------------------
# PSD master --> scaled PNGs for composition onto logo template.

##
# PSD --> PNG for master logo icon.
#
#  x28 is GitLab
#  x54 is Website
# x200 is SPI project page
# x900 is WordPress theme
#
# GM does not support PSD anymore!
#
find . -type f -name "*.psd" | cut -d'/' -f2 | while read k; do
for r in x28 x54 x200 x900; do
    magick convert -resize ${r} "${k}[0]" "gen_${k%*.psd}_${r}.png";
    chown 1000:1000 "gen_${k%*.psd}_${r}.png";
done
done

#---------------------------------------------------------------
# SVG + PSD template composition.

##
# Contrast suffers a bit but these are great on image backgrounds.
#
for c in color black white; do
for r in x28 x54 x200 x900; do
    # transparent black polyguin
    gm composite gen_polyguin_${c}_${r}.png gen_polylogo_template_black_${r}.png gen_polylogo_black_${c}_${r}.png;
    chown 1000:1000 gen_polylogo_black_${c}_${r}.png;

    # transparent white polyguin
    gm composite gen_polyguin_${c}_${r}.png gen_polylogo_template_white_${r}.png gen_polylogo_white_${c}_${r}.png;
    chown 1000:1000 gen_polylogo_white_${c}_${r}.png;
done # r
done # c

##
# These have better contrast and are suited for solid backgrounds.
#
# FIXME: improve contrast.
#
for r in x28 x54 x200 x900; do
    # solid black color --> grayscale polyguin
    magick convert -colorspace Gray gen_polylogo_black_color_${r}.png gen_polylogo_black_mono_${r}.png;
    chown 1000:1000 gen_polylogo_black_mono_${r}.png;

    # solid white color --> grayscale polyguin
    magick convert -colorspace Gray gen_polylogo_white_color_${r}.png gen_polylogo_white_mono_${r}.png;
    chown 1000:1000 gen_polylogo_black_mono_${r}.png;
done # r

#---------------------------------------------------------------
# Favicon generation.

##
# 32x32 is GitLab
# Website uses native SVG
#
magick convert -background none -gravity center -resize 32x32 -extent 32x32 gen_polyguin_color.svg icons/gen_polyguin_color_favicon.ico;
chown 1000:1000 icons/gen_polyguin_color_favicon.ico;

#---------------------------------------------------------------
# Other custom outputs.

##
# GitLab group/project avatar is 96x96 square. MUST be square.
#
magick convert -background none -gravity center -resize 96x96 -extent 96x96 gen_polyguin_color.svg gen_polyguin_color_gitlab.png;
chown 1000:1000 gen_polyguin_color_gitlab.png;

##
# Twitter avatar is 400x400 square. MUST be square.
#
magick convert -background none -gravity center -resize 400x400 -extent 400x400 gen_polyguin_color.svg gen_polyguin_color_twitter.png;
chown 1000:1000 gen_polyguin_color_twitter.png;

##
# WordPress theme screenshot is 1200x900. MUST be this.
#
magick convert -background none -gravity center -resize 1200x900 -extent 1200x900 gen_polylogo_black_color_x900.png gen_polyguin_color_wordpress.png;
chown 1000:1000 gen_polyguin_color_wordpress.png;

##
# GitHub group/project avatar is 500x500 square. MUST be square.
#
magick convert -background none -gravity center -resize 500x500 -extent 500x500 gen_polyguin_color.svg gen_polyguin_color_github.png;
chown 1000:1000 gen_polyguin_color_github.png;

#===============================================================
EOF
#===============================================================
